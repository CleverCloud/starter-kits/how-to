# Clever Cloud Starter-Kits - How to

The [Clever Cloud Starter kits](https://gitlab.com/CleverCloud/starter-kits) are an experiment created to help people to practice DevOps easily with Clever Cloud platform and GitLab application.
A "CC starter-kit" is thought to run like that:
- fork one of the starter kits (or clone it and push it again to your GitLab account)
- break the fork relationship
- set an application name and a domain in the `.gitlab-ci.yml`
- commit and push to GitLab
And the deployment starts

With that, you gain the ability to deploy easily review applications from your project to temporary VM.

**The starter kits are opensource and if you want more starter kits, more features in the CI/CD process, etc. ... Please [open an issue](https://gitlab.com/CleverCloud/starter-kits/how-to/issues)**

## All the steps in pictures

![cc-starter-kits.gif](cc-starter-kits.gif)